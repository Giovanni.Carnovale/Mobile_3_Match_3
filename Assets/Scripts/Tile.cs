using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tile : MonoBehaviour
{
    public Vector2 position
    {
        get { return position; }
        set { position = value; }
    }
    [SerializeField] GameObject[] ItemPrefabs;

    // Start is called before the first frame update
    void Start()
    {
        Initialize();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void Initialize()
    {
        int indexOfItem = Random.Range(0, ItemPrefabs.Length);
        GameObject item = GameObject.Instantiate(ItemPrefabs[indexOfItem], this.transform);

    }
}
