using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridManager : MonoBehaviour
{
    [SerializeField] GameObject TilePrefab;
    [SerializeField] const int width = 8, height = 8;

    [SerializeField] float marginHorizontal = 4f, marginVertical = 4f;

    private Tile[,] board;
    private RectTransform rt;

    // Start is called before the first frame update
    void Awake()
    {
        board = new Tile[width, height];
        rt = GetComponent<RectTransform>();

        initializeBoard();
        fill();
        avoidStartupMatch();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void initializeBoard()
    {
        float size = getSize();
        for(int row = 0; row < height; row++)
        {
            for(int col = 0; col < width; col++)
            {
                Vector2 pos = new Vector2(row, col);
                
                GameObject tile = GameObject.Instantiate(TilePrefab, this.transform);
                tile.name = " Tile " + row + ", " + col;
                RectTransform tileRt = tile.GetComponent<RectTransform>();
                tileRt.sizeDelta = getSize() * Vector2.one;
                tile.GetComponent<RectTransform>().anchoredPosition = tileRt.sizeDelta*pos + new Vector2(marginHorizontal, marginVertical);
                
            }
        }
    }

    private void fill()
    {

    }

    private float getSize()
    {
        RectTransform rt = GetComponent<RectTransform>();
        return Mathf.Min((rt.rect.width - 2*marginHorizontal) / width, (rt.rect.height - 2*marginVertical) / height);
    }

    private void avoidStartupMatch() { 

    }
}
